// WD078-16 | JavaScript - Operators
/*Activity:
1. In the S16 folder, create an activity folder, an index.html file inside of it and link the index.js file.
2. Create an index.js file and console log the message Hello World to ensure that the script file is properly associated with the html file.
3. Create two variables that will store two different number values and create several variables that will store the sum, difference, product and quotient of the two numbers. Print the results in the console
4. Using comparison operators print out a string in the console that will return a message if the sum is greater than the difference.
5. Using comparison and logical operators print out a string in the console that will return a message if the product and quotient are both positive numbers.
6. Using comparison and logical operators print out a string in the console that will return a message if one of the results is a negative number.
7. Using comparison and logical operators print out a string in the console that will return a message if one of the results is zero.*/


console.log('Hello World!');

let num1 = 30;
let num2 = 60;

let sum = num1 + num2;
console.log("The sum of the two numbers is: " + sum);

let difference = num1 - num2;
console.log("The difference of the two numbers is: " + difference);

let product = num1 * num2;
console.log("The product of the two numbers is: " + product);

let quotient = num1 / num2;
console.log("The quotient of the two numbers is: " + quotient);

let inequalities = sum > difference;
console.log("The sum is greater than the difference: " + inequalities);

let PositiveNumbers = (product > 0) && (quotient > 0);
console.log("The product and quotient are positive numbers: " + PositiveNumbers);

let NegativeNumbers = (sum < 0 || difference < 0 || product < 0 || quotient < 0) ;
console.log("One of the results is negative: " + NegativeNumbers);

let ZeroNumber = (sum !== 0 || difference !== 0 || product !==0 || quotient !== 0);
console.log("All the results are not equal to zero: " +  ZeroNumber);
